use std::borrow::Cow;
use std::cmp;
use std::collections::{BTreeMap, HashMap, HashSet};

use chrono::prelude::*;
use mysql_async::prelude::*;
use mysql_async::Pool;
use once_cell::sync::Lazy;
use rocket::futures::StreamExt;
use serde::{Deserialize, Serialize};

use crate::graphs::*;
use crate::Credentials;

pub type Namespace = i16;

/// inner vec is (namespace, num_edits), sorted by namespace ascending.
type MonthNsCount = Vec<(Namespace, u32)>;
/// outer vec is a list of months.
pub type MonthNsCounts = Vec<MonthNsCount>;

#[derive(Copy, Clone)]
pub struct ActorId(pub u64);
#[derive(Copy, Clone)]
pub struct UserId(pub u32);

const WIKIMEDIA_TIME_FORMAT: &str = "%Y%m%d%H%M%S";

static NAMESPACE_NAMES: Lazy<HashMap<Namespace, &'static str>> = Lazy::new(|| {
    HashMap::from_iter([
        (0, "Article"), // !
        (1, "Talk"),
        (2, "User"),
        (3, "User talk"),
        (4, "Wikipedia"),
        (5, "Wikipedia talk"),
        (6, "File"),
        (7, "File talk"),
        (8, "MediaWiki"),
        (9, "MediaWiki talk"),
        (10, "Template"),
        (11, "Template talk"),
        (12, "Help"),
        (13, "Help talk"),
        (14, "Category"),
        (15, "Category talk"),
        (100, "Portal"),
        (101, "Portal talk"),
        (118, "Draft"),
        (119, "Draft talk"),
        (710, "TimedText"),
        (711, "TimedText talk"),
        (828, "Module"),
        (829, "Module talk"),
        (2300, "Gadget"),
        (2301, "Gadget talk"),
        (2302, "Gadget definition"),
        (2303, "Gadget definition talk"),
    ])
});

pub fn namespace_name(ns: Namespace) -> Cow<'static, str> {
    NAMESPACE_NAMES
        .get(&ns)
        .map_or(Cow::Owned(ns.to_string()), |s| Cow::Borrowed(*s))
}

/// compute the difference in months between the two datetimes, in a manner appropriate for
/// combine_month_ns_counts
/// ```rust
/// use chrono::{Utc, TimeZone};
/// use fast_ec::queries::month_diff;
/// let d = |y, m, d| Utc.ymd(y, m, d).and_hms(0, 0, 0);
/// assert_eq!(month_diff(d(2022, 1, 27), d(2022, 2, 27)), 1);
/// assert_eq!(month_diff(d(2022, 1, 27), d(2023, 1, 27)), 12);
/// assert_eq!(month_diff(d(2022, 1, 27), d(2023, 2, 27)), 13);
/// assert_eq!(month_diff(d(2022, 9, 27), d(2023, 2, 27)), 5);
/// assert_eq!(month_diff(Utc.ymd(2010, 04, 18).and_hms(9, 31, 43), Utc.ymd(2012, 01, 18).and_hms(9, 16, 48)), 21);
/// ```
pub fn month_diff(a: DateTime<Utc>, b: DateTime<Utc>) -> usize {
    let greater = cmp::max(a, b);
    let lesser = cmp::min(a, b);
    let year_diff: i32 = greater.year() - lesser.year();
    let month_diff: i32 = greater.month() as i32 - lesser.month() as i32;
    let full_month_diff: i32 = month_diff + 12 * year_diff;
    full_month_diff.try_into().unwrap()
}

/// ```rust
/// use chrono::{Utc, TimeZone};
/// use fast_ec::queries::{MonthNsCounts, combine_month_ns_counts};
/// let time1 = Utc.ymd(2022, 1, 27).and_hms(0, 0, 0);
/// let time2 = Utc.ymd(2022, 2, 27).and_hms(0, 0, 0);
/// let time3 = Utc.ymd(2022, 3, 27).and_hms(0, 0, 0);
///
/// // (a_first_edit < b_first_edit, a_last_edit < b_last_edit)
///
/// // false, false
/// // ---b---
/// //   ---a---
/// assert_eq!(combine_month_ns_counts(
///         vec![vec![(0, 1)], vec![(0, 10)]], time2, time3,
///         vec![vec![(0, 100)], vec![(0, 1000)]], time1, time2),
///     vec![vec![(0, 100)], vec![(0, 1001)], vec![(0, 10)]]);
/// ```
pub fn combine_month_ns_counts(
    a: MonthNsCounts,
    a_first_edit: DateTime<Utc>,
    a_last_edit: DateTime<Utc>,
    b: MonthNsCounts,
    b_first_edit: DateTime<Utc>,
    b_last_edit: DateTime<Utc>,
) -> MonthNsCounts {
    assert_eq!(a.len(), month_diff(a_first_edit, a_last_edit) + 1);
    assert_eq!(b.len(), month_diff(b_first_edit, b_last_edit) + 1);

    let num_months = month_diff(a_first_edit, b_first_edit);
    let start_pad = std::iter::repeat_with(Vec::new).take(num_months);
    let end_pad = std::iter::repeat_with(Vec::new);

    let merge_months = |(month1, month2): (MonthNsCount, MonthNsCount)| {
        let mut res = Vec::with_capacity(std::cmp::max(month1.len(), month2.len()));
        let mut idx1 = 0;
        let mut idx2 = 0;
        loop {
            use std::cmp::Ordering::*;
            match (month1.get(idx1).copied(), month2.get(idx2).copied()) {
                (Some((ns1, count1)), Some((ns2, count2))) => match ns1.cmp(&ns2) {
                    Less => {
                        res.push((ns1, count1));
                        idx1 += 1;
                    }
                    Greater => {
                        res.push((ns2, count2));
                        idx2 += 1;
                    }
                    Equal => {
                        res.push((ns1, count1 + count2));
                        idx1 += 1;
                        idx2 += 1;
                    }
                },
                (Some((ns1, count1)), None) => {
                    res.push((ns1, count1));
                    idx1 += 1;
                }
                (None, Some((ns2, count2))) => {
                    res.push((ns2, count2));
                    idx2 += 1;
                }
                (None, None) => break,
            }
        }
        res
    };

    match (a_first_edit < b_first_edit, a_last_edit < b_last_edit) {
        (false, false) => start_pad
            .chain(a)
            .zip(b.into_iter().chain(end_pad))
            .map(merge_months)
            .collect(),
        (false, true) => start_pad
            .chain(a)
            .chain(end_pad)
            .zip(b)
            .map(merge_months)
            .collect(),
        (true, false) => start_pad
            .chain(b)
            .chain(end_pad)
            .zip(a)
            .map(merge_months)
            .collect(),
        (true, true) => start_pad
            .chain(b)
            .zip(a.into_iter().chain(end_pad))
            .map(merge_months)
            .collect(),
    }
}

fn combine_arrays<T: std::ops::AddAssign<T> + Copy, const N: usize>(
    mut a: [T; N],
    b: [T; N],
) -> [T; N] {
    for i in 0..N {
        a[i] += b[i];
    }
    a
}

pub const VERSION_NUMBER: u8 = 1;

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct StoredData {
    pub first_edit: DateTime<Utc>,
    pub last_edit: DateTime<Utc>,
    pub data_as_of: DateTime<Utc>,

    pub minor_edits: u32,
    pub creations: u32,

    pub page_ids_edited: HashSet<u32>,

    // starts at first_edit, ends at last_edit
    pub counts: MonthNsCounts,

    // starts on Sunday at 00:00
    #[serde(with = "serde_big_array::BigArray")]
    pub timecard: [u32; 24 * 7],
}

#[derive(Serialize)]
pub struct Statistics {
    stored_data: StoredData,

    deleted_edits: u32,
    live_edits_percent: String,
    deleted_edits_percent: String,

    avg_edits_per_day: String,
    num_days_for_average: u32,

    minor_edit_percent: String,
    avg_edits_per_page: String,

    num_edits: u32,
    num_unique_pages_edited: u32,

    namespace_totals: Vec<(Cow<'static, str>, u32)>,
    pie_chart: String,
    year_counts: String,
    month_counts: String,
    timecard: String,

    username: String,
    optin: bool,
    prev_as_of: String,
    prev_total: String,
}

impl StoredData {
    pub fn total_edits(&self) -> u32 {
        self.timecard.iter().sum()
    }

    pub fn to_statistics(
        self,
        username: &str,
        optin: bool,
        deleted_edits: u32,
        prev_as_of: Option<DateTime<Utc>>,
        prev_total: Option<u32>,
    ) -> Statistics {
        let data = self;
        let num_edits: u32 = data
            .counts
            .iter()
            .map(|month| month.iter().map(|(_namespace, num)| num).sum::<u32>())
            .sum();
        let num_days_for_average: u32 = std::cmp::max(
            data.last_edit
                .signed_duration_since(data.first_edit)
                .num_days()
                .try_into()
                .unwrap(),
            1,
        );
        let num_unique_pages_edited: u32 = data.page_ids_edited.len().try_into().unwrap();

        let mut namespace_totals = HashMap::new();
        for month in data.counts.iter() {
            for (namespace, n) in month {
                *namespace_totals.entry(*namespace).or_default() += n;
            }
        }
        let mut namespace_totals: Vec<(Namespace, u32)> = namespace_totals.into_iter().collect();
        namespace_totals.sort_unstable_by_key(|(namespace, _)| *namespace);
        let namespace_totals_str = namespace_totals
            .iter()
            .map(|(ns, n)| (namespace_name(*ns), *n))
            .collect();

        let deleted_edits_percent = 100_f32 * deleted_edits as f32 / num_edits as f32;
        let live_edits_percent = 100_f32 - deleted_edits_percent;

        Statistics {
            stored_data: data.clone(),
            deleted_edits,
            live_edits_percent: format!("{:.1}", live_edits_percent),
            deleted_edits_percent: format!("{:.1}", deleted_edits_percent),
            avg_edits_per_day: format!("{:.1}", num_edits as f32 / num_days_for_average as f32),
            num_days_for_average,
            minor_edit_percent: format!(
                "{:.1}",
                100_f32 * data.minor_edits as f32 / num_edits as f32
            ),
            avg_edits_per_page: format!("{:.1}", num_edits as f32 / num_unique_pages_edited as f32),
            num_edits,
            num_unique_pages_edited,
            namespace_totals: namespace_totals_str,
            pie_chart: pie_chart(&namespace_totals, num_edits),
            year_counts: draw_bar_chart(
                &data.counts,
                data.first_edit,
                data.last_edit,
                MonthsOrYears::Years,
            ),
            month_counts: draw_bar_chart(
                &data.counts,
                data.first_edit,
                data.last_edit,
                MonthsOrYears::Months,
            ),
            timecard: timecard(&data.counts),
            username: username.to_string(),
            optin,
            prev_as_of: prev_as_of.map_or("".into(), |d| {
                d.to_rfc3339_opts(SecondsFormat::Secs, true)
                    .replace('T', " ")
            }),
            prev_total: prev_total.map_or("".into(), |t| t.to_string()),
        }
    }
}

pub fn combine_data(a: Option<StoredData>, b: Option<StoredData>) -> Option<StoredData> {
    match (a, b) {
        (Some(a), Some(b)) => Some(StoredData {
            first_edit: cmp::min(a.first_edit, b.first_edit),
            last_edit: cmp::max(a.last_edit, b.last_edit),
            data_as_of: cmp::max(a.data_as_of, b.data_as_of),

            minor_edits: a.minor_edits + b.minor_edits,
            creations: a.creations + b.creations,

            page_ids_edited: &a.page_ids_edited | &b.page_ids_edited,
            counts: combine_month_ns_counts(
                a.counts,
                a.first_edit,
                a.last_edit,
                b.counts,
                b.first_edit,
                b.last_edit,
            ),
            timecard: combine_arrays(a.timecard, b.timecard),
        }),
        (Some(x), None) => Some(x),
        (None, Some(x)) => Some(x),
        (None, None) => None,
    }
}

/// The context needed to render the "error.html" template
#[derive(Serialize)]
struct ErrorTemplate {
    /// The error message
    error: String,
}

pub async fn actor_id_and_user_id_from_username(
    pool: &Pool,
    username: &str,
) -> Result<Option<(ActorId, UserId)>, String> {
    Ok(pool
        .get_conn()
        .await
        .map_err(|e| e.to_string())?
        .exec_first(
            "SELECT actor_id, actor_user FROM actor WHERE actor_name = ?",
            (username,),
        )
        .await
        .map_err(|e| e.to_string())?
        .map(|(a, u)| (ActorId(a), UserId(u))))
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum LiveOrDeleted {
    Live,
    Deleted,
}

fn get_query(which: LiveOrDeleted, actor_id: ActorId, since: Option<DateTime<Utc>>) -> String {
    use LiveOrDeleted::*;
    let actor_id = actor_id.0;
    let mut query = match which {
        Live => format!(
            r#"
SELECT
  rev_timestamp,
  page_namespace,
  page_id,
  rev_parent_id,
  rev_minor_edit
FROM
  revision_userindex
  JOIN `page` ON rev_page = page_id
WHERE
  rev_actor = {actor_id}"#
        ),
        Deleted => format!(
            r#"
SELECT
  ar_timestamp,
  ar_namespace,
  ar_page_id,
  ar_parent_id,
  ar_minor_edit
FROM
  archive_userindex
WHERE
  ar_actor = {actor_id}"#
        ),
    };
    if let Some(since) = since {
        query += " AND ";
        query += match which {
            Live => "rev",
            Deleted => "ar",
        };
        query += "_timestamp > ";
        query += &since.format(WIKIMEDIA_TIME_FORMAT).to_string();
    }

    query += "\nORDER BY ";
    query += match which {
        Live => "rev",
        Deleted => "ar",
    };
    query += "_timestamp ASC";

    query
}

async fn run_query(
    pool: &Pool,
    which: LiveOrDeleted,
    actor_id: ActorId,
    since: Option<DateTime<Utc>>,
) -> Option<StoredData> {
    let mut conn = pool.get_conn().await.unwrap();

    let mut first_edit: Option<DateTime<Utc>> = None;
    let mut last_edit: Option<DateTime<Utc>> = None;

    let mut minor_edits: u32 = 0;
    let mut creations: u32 = 0;

    let mut page_ids_edited: HashSet<u32> = HashSet::new();

    let mut prev_counts: MonthNsCounts = vec![];
    let mut curr_count: BTreeMap<Namespace, u32> = BTreeMap::new();

    let mut timecard: [u32; 24 * 7] = [0; 24 * 7];

    conn.query_stream(get_query(which, actor_id, since))
        .await
        .unwrap()
        .for_each(|row| {
            let (rev_timestamp, page_namespace, page_id, rev_parent_id, rev_minor_edit): (
                String,
                Namespace,
                Option<u32>,
                Option<u32>,
                bool,
            ) = row.unwrap();
            let rev_timestamp =
                chrono::NaiveDateTime::parse_from_str(&rev_timestamp, WIKIMEDIA_TIME_FORMAT)
                    .unwrap()
                    .and_local_timezone(Utc)
                    .single()
                    .unwrap();

            if let Some(last_edit) = last_edit {
                if rev_timestamp.month() != last_edit.month()
                    || rev_timestamp.year() != last_edit.year()
                {
                    prev_counts.push(curr_count.iter().map(|(&ns, &n)| (ns, n)).collect());
                    curr_count.clear();
                    for _ in 0..month_diff(rev_timestamp, last_edit).saturating_sub(1) {
                        prev_counts.push(vec![]);
                    }
                }

                // fun fact, archive_userindex does NOT yield rows in timestamp order by default
                assert!(last_edit <= rev_timestamp);
            } else {
                // this is the first edit
                first_edit = Some(rev_timestamp);
            }

            last_edit = Some(rev_timestamp);

            if rev_minor_edit {
                minor_edits += 1;
            }

            match rev_parent_id {
                Some(0) | None => {
                    creations += 1;
                }
                _ => {}
            }

            let timecard_entry_idx =
                24 * rev_timestamp.weekday().num_days_from_sunday() + rev_timestamp.hour();
            timecard[timecard_entry_idx as usize] += 1;

            if let Some(page_id) = page_id {
                page_ids_edited.insert(page_id);
            }

            *curr_count.entry(page_namespace).or_default() += 1;

            std::future::ready(())
        })
        .await;

    if !curr_count.is_empty() {
        prev_counts.push(curr_count.iter().map(|(&ns, &n)| (ns, n)).collect());
        curr_count.clear();
    }

    std::mem::drop(conn); // need this so that disconnect() returns

    match (first_edit, last_edit) {
        (Some(first_edit), Some(last_edit)) => Some(StoredData {
            first_edit,
            last_edit,
            data_as_of: Utc::now(),
            minor_edits,
            creations,
            page_ids_edited,
            counts: prev_counts,
            timecard,
        }),
        _ => None,
    }
}

async fn get_num_deleted_edits(pool: &Pool, actor_id: ActorId) -> u32 {
    let actor_id = actor_id.0;
    let mut conn = pool.get_conn().await.unwrap();
    conn.query_first(format!(
        "SELECT COUNT(*) FROM archive_userindex WHERE ar_actor = {actor_id}"
    ))
    .await
    .unwrap()
    .unwrap()
}

async fn check_optin_local(project: &str, username: &str) -> bool {
    let res = reqwest::get(format!("https://{project}/w/api.php?action=query&prop=revisions&titles=User:{username}/EditCounterOptIn.js&rvslots=main&rvprop=size&format=json&formatversion=2")).await.unwrap().json::<serde_json::Value>().await.unwrap();
    let page = &res["query"]["pages"][0];
    if page.get("missing").is_some() {
        false
    } else if page["revisions"][0]["size"].as_i64().unwrap() > 0 {
        true
    } else {
        panic!()
    }
}

async fn check_optin_global(username: &str) -> bool {
    check_optin_local("meta.wikimedia.org", username).await
}

fn conn_str(db_name: &str, credentials: &Credentials) -> String {
    let db_name = db_name.trim_end_matches("_p");
    if std::env::var("FAST_EC_USE_LOCAL_CREDS").map_or(false, |val| !val.is_empty()) {
        // (1) run ssh -N -L 4711:XXXXwiki.analytics.db.svc.wikimedia.cloud:3306 aperson@login.toolforge.org
        // (2) run ssh -N -L 4712:metawiki.analytics.db.svc.wikimedia.cloud:3306 aperson@login.toolforge.org
        // (3) copy your replica.my.cnf to the crate root (i.e. wherever you're typing "cargo r" from)
        format!(
            "mysql://{}:{}@127.0.0.1:{}/{db_name}_p",
            credentials.username,
            credentials.password,
            if db_name == "metawiki" { 4712 } else { 4711 }
        )
    } else {
        format!(
            "mysql://{}:{}@{db_name}.web.db.svc.wikimedia.cloud:3306/{db_name}_p",
            credentials.username, credentials.password,
        )
    }
}

pub fn get_pool(project: &str, credentials: &Credentials) -> Pool {
    let db_name = match project {
        "en.wikipedia.org" => "enwiki",
        "test.wikipedia.org" => "testwiki",
        "meta.wikimedia.org" => "metawiki",
        _ => todo!("{}", project),
    };
    mysql_async::Pool::new(conn_str(db_name, credentials).as_str())
}

pub struct QueriesResult {
    pub data: Option<StoredData>,
    pub num_deleted_edits: u32,
    pub optin: bool,
}

pub async fn go(
    actor_id: ActorId,
    project: &str,
    username: &str,
    pool: &Pool,
    since: Option<DateTime<Utc>>,
) -> QueriesResult {
    let (live, deleted, num_deleted_edits, optin_local, optin_global) =
        rocket::futures::future::join5(
            run_query(pool, LiveOrDeleted::Live, actor_id, since),
            run_query(pool, LiveOrDeleted::Deleted, actor_id, since),
            get_num_deleted_edits(pool, actor_id),
            check_optin_local(project, username),
            check_optin_global(username),
        )
        .await;
    QueriesResult {
        data: combine_data(live, deleted),
        num_deleted_edits,
        optin: optin_local || optin_global,
    }
}
