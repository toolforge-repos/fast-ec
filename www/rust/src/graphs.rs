use std::collections::HashMap;
use std::collections::HashSet;

use chrono::prelude::*;
use once_cell::sync::Lazy;
use plotters::style::text_anchor::{HPos, Pos, VPos};
use plotters::{prelude::*, style::RGBColor};

use crate::queries::{namespace_name, MonthNsCounts, Namespace, StoredData};

static NAMESPACE_COLORS: Lazy<HashMap<Namespace, RGBColor>> = Lazy::new(|| {
    [
        // from
        // https://github.com/x-tools/xtools/blob/f69d1a0ae7990d409c154a6c569b7f7376d7093e/src/Twig/AppExtension.php#L283
        (0, "FF5555"),
        (1, "55FF55"),
        (2, "FFEE22"),
        (3, "FF55FF"),
        (4, "5555FF"),
        (5, "55FFFF"),
        (6, "C00000"),
        (7, "0000C0"),
        (8, "008800"),
        (9, "00C0C0"),
        (10, "FFAFAF"),
        (11, "808080"),
        (12, "00C000"),
        (13, "404040"),
        (14, "C0C000"),
        (15, "C000C0"),
        (90, "991100"),
        (91, "99FF00"),
        (92, "000000"),
        (93, "777777"),
        (100, "75A3D1"),
        (101, "A679D2"),
        (102, "660000"),
        (103, "000066"),
        (104, "FAFFAF"),
        (105, "408345"),
        (106, "5c8d20"),
        (107, "e1711d"),
        (108, "94ef2b"),
        (109, "756a4a"),
        (110, "6f1dab"),
        (111, "301e30"),
        (112, "5c9d96"),
        (113, "a8cd8c"),
        (114, "f2b3f1"),
        (115, "9b5828"),
        (116, "002288"),
        (117, "0000CC"),
        (118, "99FFFF"),
        (119, "99BBFF"),
        (120, "FF99FF"),
        (121, "CCFFFF"),
        (122, "CCFF00"),
        (123, "CCFFCC"),
        (200, "33FF00"),
        (201, "669900"),
        (202, "666666"),
        (203, "999999"),
        (204, "FFFFCC"),
        (205, "FF00CC"),
        (206, "FFFF00"),
        (207, "FFCC00"),
        (208, "FF0000"),
        (209, "FF6600"),
        (250, "6633CC"),
        (251, "6611AA"),
        (252, "66FF99"),
        (253, "66FF66"),
        (446, "06DCFB"),
        (447, "892EE4"),
        (460, "99FF66"),
        (461, "99CC66"),
        (470, "CCCC33"),
        (471, "CCFF33"),
        (480, "6699FF"),
        (481, "66FFFF"),
        (484, "07C8D6"),
        (485, "2AF1FF"),
        (486, "79CB21"),
        (487, "80D822"),
        (490, "995500"),
        (491, "998800"),
        (710, "FFCECE"),
        (711, "FFC8F2"),
        (828, "F7DE00"),
        (829, "BABA21"),
        (866, "FFFFFF"),
        (867, "FFCCFF"),
        (1198, "FF34B3"),
        (1199, "8B1C62"),
        (2300, "A900B8"),
        (2301, "C93ED6"),
        (2302, "8A09C1"),
        (2303, "974AB8"),
        (2600, "000000"),
    ]
    .into_iter()
    .map(|(ns, c)| {
        (
            ns,
            RGBColor(
                u8::from_str_radix(&c[0..2], 16).unwrap(),
                u8::from_str_radix(&c[2..4], 16).unwrap(),
                u8::from_str_radix(&c[4..6], 16).unwrap(),
            ),
        )
    })
    .collect()
});

fn namespace_color(ns: Namespace) -> RGBColor {
    NAMESPACE_COLORS
        .get(&ns)
        .copied()
        .unwrap_or(RGBColor(128, 128, 128))
}

pub fn pie_chart(counts: &Vec<(Namespace, u32)>, num_edits: u32) -> String {
    let data: Vec<(Namespace, f32)> = counts
        .iter()
        .map(|(ns, n)| (*ns, 100_f32 * *n as f32 / num_edits as f32))
        .collect();
    let sizes: Vec<f64> = data
        .iter()
        .map(|(_ns, fraction)| *fraction as f64)
        .collect();
    let colors: Vec<RGBColor> = data
        .iter()
        .map(|(ns, _fraction)| namespace_color(*ns))
        .collect();
    let labels: Vec<String> = data
        .iter()
        .zip(counts)
        .map(|((ns, _fraction), (_ns, count))| format!("{} ({})", namespace_name(*ns), count))
        .collect();

    let mut buf = String::new();
    let drawing_area = SVGBackend::with_string(&mut buf, (1000, 500)).into_drawing_area();
    let mut pie = Pie::new(&(500, 250), &200_f64, &sizes, &colors, &labels);
    pie.start_angle(-90.0);
    pie.label_style(("sans-serif", 24));
    drawing_area.draw(&pie).unwrap();
    drawing_area.present().unwrap();
    std::mem::drop(drawing_area);

    buf.replace("<svg", r#"<svg id="namespace-totals""#)
}

#[derive(PartialEq, Eq, Debug)]
struct MyRectangle {
    x0: u32,
    x1: u32,
    y: usize,
    namespace: Namespace,
}

#[derive(PartialEq, Eq, Debug)]
struct GraphData {
    rects: Vec<MyRectangle>,
    totals: Vec<u32>,
}

pub enum MonthsOrYears {
    Months,
    Years,
}

fn year_counts_data(
    month_counts: &MonthNsCounts,
    first_edit: DateTime<Utc>,
    last_edit: DateTime<Utc>,
) -> GraphData {
    fn merge_months(months: &[Vec<(Namespace, u32)>]) -> Vec<(Namespace, u32)> {
        let mut totals = HashMap::new();
        for month in months {
            for (namespace, n) in month {
                *totals.entry(*namespace).or_default() += n;
            }
        }
        let mut totals: Vec<(Namespace, u32)> = totals.into_iter().collect();
        totals.sort_unstable_by_key(|(namespace, _n)| *namespace);
        totals
    }

    // all this junk is to handle when the user started editing halfway through
    // a year, so we have to handle those months as a year themselves, and only
    //  after that can we start taking months 12 at a time to form years.
    let num_months_at_beginning =
        std::cmp::min(13 - first_edit.month() as usize, month_counts.len());
    let first_year_counts = merge_months(&month_counts[..num_months_at_beginning]);
    let remaining_years_counts = month_counts[num_months_at_beginning..]
        .chunks(12)
        .map(merge_months);
    let all_years_counts: Vec<Vec<(Namespace, u32)>> = std::iter::once(first_year_counts)
        .chain(remaining_years_counts)
        .collect();

    let total_edits_each_year = all_years_counts
        .iter()
        .map(|year| year.iter().map(|(_ns, n)| *n).sum())
        .collect::<Vec<_>>();

    let first_year: u32 = first_edit.year().try_into().unwrap();
    let last_year: u32 = last_edit.year().try_into().unwrap();
    GraphData {
        rects: (first_year..last_year + 1)
            .zip(all_years_counts)
            .flat_map(|(year, year_data)| {
                let mut running_total = 0;
                year_data
                    .iter()
                    .map(move |(ns, n)| {
                        let rect = MyRectangle {
                            x0: running_total,
                            x1: running_total + n,
                            y: (year - first_year) as usize,
                            namespace: *ns,
                        };
                        running_total += n;
                        rect
                    })
                    .collect::<Vec<_>>()
            })
            .collect(),
        totals: total_edits_each_year,
    }
}
fn month_counts_data(month_counts: &MonthNsCounts) -> GraphData {
    let mut totals = Vec::with_capacity(month_counts.len());
    let mut rects = Vec::with_capacity(month_counts.len()); // this is a bad estimate
    for (month_idx, month) in month_counts.iter().enumerate() {
        let mut running_total = 0;
        for &(namespace, n) in month {
            rects.push(MyRectangle {
                x0: running_total,
                x1: running_total + n,
                y: month_idx,
                namespace,
            });
            running_total += n;
        }
        totals.push(running_total);
    }
    GraphData { rects, totals }
}

pub fn draw_bar_chart(
    month_counts: &MonthNsCounts,
    first_edit: DateTime<Utc>,
    last_edit: DateTime<Utc>,
    months_or_years: MonthsOrYears,
) -> String {
    use MonthsOrYears::*;
    let GraphData { rects, totals } = match months_or_years {
        Months => month_counts_data(month_counts),
        Years => year_counts_data(month_counts, first_edit, last_edit),
    };
    let mut buf = String::new();
    let height = totals.len() * 35 + 100;
    let drawing_area = SVGBackend::with_string(&mut buf, (1000, height as u32)).into_drawing_area();

    let label_text_style = &("sans-serif", 24).into_text_style(&drawing_area);

    let time_period_pixel_width = drawing_area
        .estimate_text_size(
            match months_or_years {
                Months => "2022-02",
                Years => "2022",
            },
            label_text_style,
        )
        .unwrap()
        .0 as i32;

    // compute maximum width in pixels of the total edits for any time period
    let max_total = totals.iter().max().unwrap();
    let max_total_pixel_width = drawing_area
        .estimate_text_size(&max_total.to_string(), label_text_style)
        .unwrap()
        .0 as i32;

    const MARGIN_BTWN_TIME_AND_TOTAL: i32 = 22;

    let actual_y_max = std::cmp::max(2, totals.len());
    let actual_y_range = (0..actual_y_max).into_segmented();
    let x_range = 0..*max_total;
    let mut ctx = ChartBuilder::on(&drawing_area)
        .set_label_area_size(
            LabelAreaPosition::Left,
            time_period_pixel_width + max_total_pixel_width + MARGIN_BTWN_TIME_AND_TOTAL,
        )
        .set_label_area_size(LabelAreaPosition::Bottom, 40)
        .margin(10)
        .build_cartesian_2d(x_range, actual_y_range)
        .unwrap();

    ctx.configure_mesh()
        .y_label_formatter(&|_| "".into()) // turn off y-axis labels; we'll drawing them ourselves
        .set_tick_mark_size(LabelAreaPosition::Left, 0)
        .x_label_style(("sans-serif", 24).into_text_style(&drawing_area))
        .draw()
        .unwrap();

    // draw y-axis labels. these contain both the month (or year), and the total edits for the
    // time period
    let first_edit_year: usize = first_edit.year() as usize;
    let first_edit_month_corrected: usize = first_edit.month() as usize - 1;
    for (i, total) in totals.iter().enumerate() {
        let y_backend_coord = ctx.backend_coord(&(0, SegmentValue::CenterOf(i))).1;
        let date_description = match months_or_years {
            MonthsOrYears::Months => {
                format!(
                    "{}-{:02}",
                    first_edit_year + (first_edit_month_corrected + i) / 12,
                    ((first_edit_month_corrected + i) % 12) + 1,
                )
            }
            MonthsOrYears::Years => (first_edit.year() as usize + i).to_string(),
        };

        drawing_area
            .draw_text(
                &date_description,
                &label_text_style.pos(Pos::new(HPos::Left, VPos::Center)),
                (0, y_backend_coord),
            )
            .unwrap();

        drawing_area
            .draw_text(
                &total.to_string(),
                &label_text_style.pos(Pos::new(HPos::Right, VPos::Center)),
                (
                    time_period_pixel_width + max_total_pixel_width + MARGIN_BTWN_TIME_AND_TOTAL,
                    y_backend_coord,
                ),
            )
            .unwrap();
    }

    let rects = rects.into_iter().map(
        |MyRectangle {
             x0,
             x1,
             y,
             namespace,
         }| {
            let mut res = Rectangle::new(
                [
                    (x0, SegmentValue::Exact(y)),
                    (x1, SegmentValue::Exact(y + 1)),
                ],
                namespace_color(namespace).filled(),
            );
            res.set_margin(5, 5, 0, 0);
            res
        },
    );
    ctx.draw_series(rects).unwrap();

    // hack: so for some reason the graph thing gives us an extra y-value at the top, so we draw a
    // white rectangle over it
    drawing_area
        .draw(&Rectangle::new([(0, 0), (1000, 45)], WHITE.filled()))
        .unwrap();

    drawing_area.present().unwrap();
    std::mem::drop(ctx);
    std::mem::drop(drawing_area);

    buf
}

pub fn timecard(_counts: &MonthNsCounts) -> String {
    "".into()
}

pub fn data1() -> StoredData {
    StoredData {
        first_edit: Utc.ymd(2022, 10, 27).and_hms(0, 0, 0),
        last_edit: Utc.ymd(2022, 10, 27).and_hms(0, 0, 0),
        data_as_of: Utc.ymd(2022, 10, 27).and_hms(0, 0, 0),
        minor_edits: 1,
        creations: 1,
        page_ids_edited: HashSet::from_iter([1]),
        counts: vec![vec![(0, 1)]],
        timecard: [1; 24 * 7],
    }
}

pub fn data2() -> StoredData {
    StoredData {
        first_edit: Utc.ymd(2021, 12, 27).and_hms(0, 0, 0),
        last_edit: Utc.ymd(2022, 1, 27).and_hms(0, 0, 0),
        data_as_of: Utc.ymd(2022, 10, 27).and_hms(0, 0, 0),
        minor_edits: 1,
        creations: 1,
        page_ids_edited: HashSet::from_iter([1]),
        counts: vec![vec![(0, 1)], vec![(0, 1)]],
        timecard: [1; 24 * 7],
    }
}
pub fn data3() -> StoredData {
    StoredData {
        first_edit: Utc.ymd(2022, 10, 27).and_hms(0, 0, 0),
        last_edit: Utc.ymd(2022, 10, 27).and_hms(1, 0, 0),
        data_as_of: Utc.ymd(2022, 10, 27).and_hms(1, 0, 0),
        minor_edits: 1,
        creations: 1,
        page_ids_edited: HashSet::from_iter([1]),
        counts: vec![vec![(0, 1), (1, 1)]],
        timecard: [1; 24 * 7],
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_year_counts_1() {
        let StoredData {
            first_edit,
            last_edit,
            counts,
            ..
        } = data1();
        assert_eq!(
            year_counts_data(&counts, first_edit, last_edit),
            GraphData {
                rects: vec![MyRectangle {
                    x0: 0,
                    x1: 1,
                    y: 0,
                    namespace: 0
                }],
                totals: vec![1]
            }
        );
    }

    #[test]
    fn test_year_counts_2() {
        let StoredData {
            first_edit,
            last_edit,
            counts,
            ..
        } = data2();
        assert_eq!(
            year_counts_data(&counts, first_edit, last_edit),
            GraphData {
                rects: vec![
                    MyRectangle {
                        x0: 0,
                        x1: 1,
                        y: 0,
                        namespace: 0
                    },
                    MyRectangle {
                        x0: 0,
                        x1: 1,
                        y: 1,
                        namespace: 0
                    }
                ],
                totals: vec![1, 1]
            }
        );
    }

    #[test]
    fn test_year_counts_3() {
        let StoredData {
            first_edit,
            last_edit,
            counts,
            ..
        } = data3();
        assert_eq!(
            year_counts_data(&counts, first_edit, last_edit),
            GraphData {
                rects: vec![
                    MyRectangle {
                        x0: 0,
                        x1: 1,
                        y: 0,
                        namespace: 0
                    },
                    MyRectangle {
                        x0: 1,
                        x1: 2,
                        y: 0,
                        namespace: 1
                    }
                ],
                totals: vec![2]
            }
        );
    }
}
