use sled::{IVec, Tree};

use crate::queries::{self, StoredData, UserId};

fn serialize_data(data: &StoredData) -> IVec {
    let mut data = postcard::to_stdvec(&data).unwrap();
    data.insert(0, queries::VERSION_NUMBER);
    data.into()
}

fn deserialize_data(data: IVec) -> StoredData {
    assert_eq!(data[0], queries::VERSION_NUMBER);
    postcard::from_bytes(&data[1..]).unwrap()
}

fn make_key(project: &str, user_id: UserId) -> Vec<u8> {
    let mut key = project.as_bytes().to_vec();
    key.extend(user_id.0.to_be_bytes());
    key
}

pub fn fetch(cache: &Tree, project: &str, user_id: UserId) -> Option<StoredData> {
    cache
        .get(make_key(project, user_id))
        .unwrap()
        .map(deserialize_data)
}

pub fn put(cache: &Tree, project: &str, user_id: UserId, data: &StoredData) {
    cache
        .insert(make_key(project, user_id), serialize_data(data))
        .unwrap();
}
